1) SELECT customerName FROM customers WHERE country = "Philippines";

2) SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

3) SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

4) SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

5) SELECT customerName FROM customers WHERE state IS NULL;

6) SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

7) SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

8) SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

9) SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

10) SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

11) SELECT DISTINCT country FROM customers;

12) SELECT DISTINCT status FROM orders;

13) SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

14) SELECT firstName, lastName, officeCode FROM employees WHERE officeCode = 5;

15) SELECT customerName FROM customers WHERE salesRepEmployeeNumber = 1166;

16) SELECT p.productName, c.customerName 
	FROM customers AS c
	JOIN orders as O 
		ON o.customerNumber = c.customerNumber
	JOIN orderdetails as od
		ON od.orderNumber = o.orderNumber
	JOIN products AS p
		ON od.productCode = p.productCode
	WHERE c.customerName = "Baane Mini Imports";

17) SELECT e.firstName, e.lastName, c.customerName, o.country
	FROM employees AS e
	JOIN customers AS c
		ON e.employeeNumber = c.salesRepEmployeeNumber
	JOIN offices AS o 
		ON e.officeCode = o.officeCode;

18) SELECT lastName, firstName FROM employees WHERE reportsTo = 1143;

19) SELECT productName, MSRP FROM products ORDER BY MSRP DESC LIMIT 1;

20) SELECT DISTINCT COUNT(customerNumber) AS numberOfCustomers FROM customers WHERE country = "UK";

21) SELECT productLine, COUNT(*) AS numberOfProducts FROM products GROUP BY productLine;

22) SELECT salesRepEmployeeNumber, COUNT(*) AS customersServed FROM customers GROUP BY salesRepEmployeeNumber;

23) SELECT productName, quantityInStock FROM products WHERE productLine = "planes" AND quantityInStock < 1000;